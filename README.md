# DeadZone command dice roller

This is a simple pure JS application that provides an alternative way to roll the command dice.

## Current functionality:

- Press "Roll" to roll all the dice.
- Press "Reroll" to reroll all unfixed dice.
- Press "Add die" to add a new die.
- Press "Remove die" to remove the last added die.
- Press a die to fix it, press it once again to unfix it.
- Press "Command mode" to toggle the "Command mode". In "Command mode" you can toggle dice as they are used during the turn.
Press "Command mode" once again to return to rolling. The used dice tags are reset.
