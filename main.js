diceSides = [];
dice = [];
commandMode = false;

function newDie(){
  var die = {};
  die.side=0;
  die.hold=false;
  die.activated=false;
  return die;
}

function preload() {
    for (var i = 0; i < arguments.length; i++) {
        diceSides[i] = new Image();
        diceSides[i].src = preload.arguments[i];
    }
}

function init(){
  preload("img/dice.png", "img/fight.png", "img/move.png",
    "img/plusone.png", "img/shoot.png", "img/splat.png");
  addDie();
  addDie();
  addDie();
  roll();
}

function addDie(){
  dice.push(newDie());
  render();
}

function deleteDie(){
  if (dice.length>0) {dice.pop()}
  render();
}

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function roll(hold=false){
  dice.forEach(function(item){
    if (!(hold && item.hold)){
      item.side=getRandomInt(6);
    }
    if (!hold) { item.hold=false }
  })
  render();
}

function render(){
  var diceBox = document.getElementById("dice");
  while (diceBox.children.length!=dice.length) {
    if (diceBox.children.length>dice.length){
      diceBox.removeChild(diceBox.lastChild);
    } else {
      let node = document.createElement("img");
      node.setAttribute("onclick", "dieClicked(this)");
      diceBox.appendChild(node);
    }
  }
  if (commandMode){
    for(i=0;i<diceBox.children.length;i++){
      let node = diceBox.children.item(i);
      node.setAttribute("class", "die")
      if (dice[i].activated){
        node.classList.add("commandUsed");
      }
    }
  } else {
    for(i=0;i<diceBox.children.length;i++){
      let node = diceBox.children.item(i);
      node.src = diceSides[dice[i].side].src;
      node.setAttribute("class", "die");
      if (dice[i].hold) {
        node.classList.add("held");
      } else {
        node.classList.add("free");
      }
      node.setAttribute("dieNumber", i);
    }
  }
}

function switchMode(){
  buttons = document.getElementById("buttons").children;
  for(i=0;i<buttons.length;i++){
    node = buttons.item(i);
    if (node.classList.contains("rollMode")){
      node.classList.toggle("commandMode");
    }
  }
  dice.forEach(function(item){
    item.activated=false;
  })
  document.getElementById("switchMode").classList.toggle("commandModeActive")
  commandMode = !commandMode;
  render();
}

function dieClicked(node){
  if (commandMode){
    dice[node.getAttribute("dieNumber")].activated = true;
  } else {
    dice[node.getAttribute("dieNumber")].hold = !(dice[node.getAttribute("dieNumber")].hold);
  }

  render();
}

window.onload = function(){
  init();
}
